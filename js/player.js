let player;

function Player(classType, health, mana, strength, agility, speed) {
    this.classType = classType;
    this.health = health;
    this.mana = mana;
    this.strength = strength;
    this.agility = agility;
    this.speed = speed;
}

let PlayerMoves = {
    calcAttack: function() {
        // whom will attack first?
        let getPlayerSpeed = player.speed;
        let getEnemySpeed = enemy.speed;
        // player attacks
        let playerAttack = function() {
            let calcBaseDamage;
            if (player.mana > 0) {
                calcBaseDamage = player.mana / 100;
            } else {
                calcBaseDamage = (player.strength + player.agility/2) /100;
            }
            let offsetDamage = Math.floor(Math.random() * Math.floor(10));
            let calcOutputDamage = calcBaseDamage + offsetDamage;
            // Number of hits
            let numberOfHits = Math.floor(Math.random() * Math.floor((player.speed + player.agility/2) / 10) /2) + 1;
            let attackValues = [calcOutputDamage, numberOfHits];
            return attackValues;
        }
    
        // Enemy attacks
        let enemyAttack = function() {
            let calcBaseDamage;
            if (enemy.mana > 0) {
                calcBaseDamage = enemy.mana / 100;
            } else {
                calcBaseDamage = enemy.strength * enemy.agility / 1000;
            }
            let offsetDamage = Math.floor(Math.random() * Math.floor(10));
            let calcOutputDamage = calcBaseDamage + offsetDamage;
            // Number of hits
            let numberOfHits = Math.floor(Math.random() * Math.floor(player.agility / 10 ) / 2 ) + 1;
            let attackValues = [calcOutputDamage, numberOfHits];
            return attackValues;
        }
        let getPlayerHealth = document.querySelector(".health-player");
        let getEnemyHealth = document.querySelector(".health-enemy");
        // Initiate attacks:
        if (getPlayerSpeed >= getEnemySpeed) {
            let playerAttackValues = playerAttack();
            let totalDamage = playerAttackValues[0] * playerAttackValues[1];
            enemy.health -= totalDamage;
            alert("You hit for " + playerAttackValues[0] * playerAttackValues[1] + " ( " + playerAttackValues[0] + " damage " + playerAttackValues[1] + " times.)");
            if (enemy.health <= 0) {
                alert("You win! Refresh browser to play again.");
                getPlayerHealth.innerHTML = 'Health: ' + player.health;
                getEnemyHealth.innerHTML = 'Health: 0';
            } else {
                getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                //enemy attacks
                let enemyAttackValues = enemyAttack();
                let totalDamage = enemyAttackValues[0] * enemyAttackValues[1];
                player.health -= totalDamage;
                alert("Enemy hits for " + enemyAttackValues[0] * enemyAttackValues[1] + " ( " + enemyAttackValues[0] + " damage " + enemyAttackValues[1] + " times.)");
                if (player.health <= 0) {
                    alert("You lose! Refresh browser to play again.");
                    getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                    getPlayerHealth.innerHTML = 'Health: 0';
                } else {
                    getPlayerHealth.innerHTML = 'Health: ' + player.health;
                }
            }
        } else if (getPlayerSpeed < getEnemySpeed) {
            let enemyAttackValues = enemyAttack();
            let totalDamage = enemyAttackValues[0] * enemyAttackValues[1];
            player.health -= totalDamage;
            alert("Enemy hits for " + enemyAttackValues[0] * enemyAttackValues[1] + " ( " + enemyAttackValues[0] + " damage " + enemyAttackValues[1] + " times.)");
            if (player.health <= 0) {
                alert("You lose! Refresh browser to play again.");
                getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                getPlayerHealth.innerHTML = 'Health: 0';
            } else {
                getPlayerHealth.innerHTML = 'Health: ' + player.health;
                //enemy attacks
                let playerAttackValues = playerAttack();
                let totalDamage = playerAttackValues[0] * playerAttackValues[1];
                enemy.health -= totalDamage;
                alert("You hit for " + playerAttackValues[0] * playerAttackValues[1] + " ( " + playerAttackValues[0] + " damage " + playerAttackValues[1] + " times.)");
                if (enemy.health <= 0) {
                    alert("You win! Refresh browser to play again.");
                    getPlayerHealth.innerHTML = 'Health: ' + player.health;
                    getEnemyHealth.innerHTML = 'Health: 0';
                } else {
                    
                    getEnemyHealth.innerHTML = 'Health: ' + enemy.health;
                }
            }
        }
    }
}