let GameManager = {
    setGameStart: function(classType) {
        this.resetPlayer(classType);
        this.setPreFight();
    },
    resetPlayer: function(classType) {
        //NORMALIZE DEM VALUES LATER, ballance issues
        switch (classType) {
            case "FireMage":
                player = new Player(classType, 100, 300, 20, 50, 100);
                break;
            case "FrostMage":
                player = new Player(classType, 250, 200, 35, 35, 80);
                break;
            case "ArcaneMage":
                player = new Player(classType, 70, 500, 15, 40, 170);
                break;
            case "Rogue":
                player = new Player(classType, 180, 0, 60, 120, 140);
                break;
        }
        let getInterface = document.querySelector(".interface");
        getInterface.innerHTML = '<img src="img/avatars-player/' + classType.toLowerCase() + '.jpeg" class="img-avatar"><div><h3>' + classType + '</h3><p class="health-player">Health: ' + player.health + '</p><p>Mana: ' + player.mana + '</p><p>Strength: ' + player.strength + '</p><p>Agility: ' + player.agility + '</p><p>Speed: ' + player.speed + '</p></div>';
    },
    setPreFight: function() {
        let getHeader = document.querySelector(".header");
        let getActions = document.querySelector(".actions");
        let getArena = document.querySelector(".arena");
        getHeader.innerHTML = '<p>Task: Find an enemy!</p>';
        getActions.innerHTML = '<a href="#" class="btn-prefight" onclick="GameManager.setFight()">Search for an enemy!</a>';
        getArena.style.visibility = "visible";
    },
    setFight: function() {
        let getHeader = document.querySelector(".header");
        let getActions = document.querySelector(".actions");
        let getEnemy= document.querySelector(".enemy");
        // CreateEnemy
        let enemy00 = new Enemy("Doge", 150, 0, 70, 70, 100);
        let enemy01 = new Enemy("Shibe", 120, 0, 70, 70, 130);
        let chooseRandomEnemy = Math.floor(Math.random() *Math.floor(2));
        switch (chooseRandomEnemy) {
            case 0:
                enemy = enemy00;
                break;
            case 1:
                enemy = enemy01;
                break;
        }
        getHeader.innerHTML = '<p>Task: Choose your move.</p>'
        getActions.innerHTML = '<a href="#" class="btn-prefight" onclick="PlayerMoves.calcAttack()">Attack!</a>';
        getEnemy.innerHTML = '<img src="img/avatars-enemies/' + enemy.enemyType.toLowerCase() + '.jpeg" class="img-avatar"><div><h3>' + enemy.enemyType + '</h3><p class="health-enemy">Health: ' + enemy.health + '</p><p>Mana: ' + enemy.mana + '</p><p>Strength: ' + enemy.strength + '</p><p>Agility: ' + enemy.agility + '</p><p>Speed: ' + enemy.speed + '</p></div>';

    }
}